name:           attest-tools
Version:        0.2.92
Release:        8
Summary:        Attestation tools

Source0:        https://gitee.com/openeuler/%{name}/repository/archive/v%{version}.tar.gz
Patch0:		attest-tools-support-openssl-3.0.patch

License:        GPL-2.0-or-later
Url:            https://gitee.com/openeuler/attest-tools
BuildRequires:  autoconf automake libcurl-devel libtool openssl-devel
BuildRequires:  digest-list-tools-devel json-c-devel libcurl-devel tss2-devel systemd
Requires:       json-c curl tss2 openssl_tpm2_engine

%if 0%{?suse_version}
BuildRequires:  libopenssl-devel
%else
BuildRequires:  openssl-devel
%endif

%description
This package includes the tools to perform remote attestation with a quote
or a TPM key.

%package devel
Summary: Development headers and libraries for %{name}
Requires:%{name} = %{version}-%{release}

%description devel
This package includes the headers of the libraries.

%prep
%autosetup -n %{name}-v%{version} -p1

%build
autoreconf -iv
%configure
%make_build

%install
%make_install
%delete_la
install -m 755 -d $RPM_BUILD_ROOT/etc/attest-tools/ek_ca_certs
install -m 755 -d $RPM_BUILD_ROOT/etc/attest-tools/privacy_ca_certs
install -m 755 -d $RPM_BUILD_ROOT/etc/sysconfig
install -m 644 etc/attest_ra_server %{buildroot}/etc/sysconfig/attest_ra_server
install -m 644 etc/attest_tls_server %{buildroot}/etc/sysconfig/attest_tls_server

%files
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/ek_ca_certs
%dir %{_sysconfdir}/%{name}/privacy_ca_certs
%{_sysconfdir}/%{name}/req_examples/*
%config(noreplace) %{_sysconfdir}/sysconfig/attest_ra_server
%config(noreplace) %{_sysconfdir}/sysconfig/attest_tls_server
%{_unitdir}/attest_ra_server.service
%{_unitdir}/attest_tls_server.service
%{_libdir}/libenroll_client.so
%{_libdir}/libverifier_ima_policy.so
%{_libdir}/libskae.so
%{_libdir}/libverifier_bios.so
%{_libdir}/libattest.so
%{_libdir}/libverifier_dummy.so
%{_libdir}/libenroll_server.so
%{_libdir}/libverifier_ima_cp.so
%{_libdir}/libverifier_ima_sig.so
%{_libdir}/libverifier_evm_key.so
%{_libdir}/libeventlog_ima.so
%{_libdir}/libverifier_ima_boot_aggregate.so
%{_libdir}/libeventlog_bios.so
%{_bindir}/attest_tls_client
%{_bindir}/attest_build_json
%{_bindir}/attest_tls_server
%{_bindir}/attest_ra_server
%{_bindir}/attest_ra_client
%{_bindir}/attest_create_skae
%{_bindir}/attest_certify.sh
%{_bindir}/ekcert_read.sh
%{_bindir}/attest_parse_json
%{_bindir}/get_pgp_keys.sh
%{_bindir}/generate_demoCA.sh

%files devel
%{_includedir}/attest-tools

%changelog
* Mon Jan 27 2025 Funda Wang <fundawang@yeah.net> - 0.2.92-8
- remove libtool .la files

* Thu Mar 16 2023 luhuaxin <luhuaxin1@huawei.com> - 0.2.92-7
- Support openssl 3.0

* Tue Feb 7 2023 gaoyusong <gaoyusong2@huawei.com> - 0.2.92-6
- Fix build error with openssl

* Thu Nov 17 2022 luhuaxin <luhuaxin1@huawei.com> - 0.2.92-5
- use release source pkg

* Fri Sep 03 2021 wangyue <wangyue92@huawei.com> - 0.2.92-4
- Add openssl_tpm2_engine in requires

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 0.2.92-3
- Fix building error: File must begin with "/": %{_unitdir}/attest_ra_server.service
- Add systemd in BuildRequires

* Tue Mar 30 2021 Anakin Zhang <zhangtianxing3@huawei.com> - 0.2.92-2
- adjust build requires to digest-list-tools-devel

* Wed Feb 10 2021 Roberto Sassu <roberto.sassu@huawei.com> - 0.2.92-1
- Remove dependency on openssl_tpm2_engine
- Add support for PGP keys
- Move configuration files to /etc/attest-tools
- Obtain CA files from openssl configuration
- Make primary key persistent
- Add systemd units and requirements examples
- Add SKAE DATA URL extension to CSR
- Bug fixes

* Fri Nov 13 2020 Roberto Sassu <roberto.sassu@huawei.com> - 0.2.91-1
- Update algorithm for boot_aggregate calculation
- Install includes
- Bug fixes

* Mon Sep 14 2020 Roberto Sassu <roberto.sassu@huawei.com> - 0.2.90-2
- Change Source0 in spec file

* Wed Jul 08 2020 Roberto Sassu <roberto.sassu@huawei.com> - 0.2.90
- Bug fixes

* Thu Dec 12 2019 Roberto Sassu <roberto.sassu@huawei.com> - 0.2.0
- Add quote protocol
- Add parser for TPM 2.0 event log
- Add evm_key and dummy verifiers
- Add creation of certificate for TPM key
- Add creation of symmetric key to be used with EVM
- Add ima-sig and ima-cp verifiers

* Fri Aug 16 2019 Roberto Sassu <roberto.sassu@huawei.com> - 0.1.0
- First public release
